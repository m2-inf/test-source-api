<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\components\ApiActiveController;
use api\modules\v1\models\search\UserSearch;
use api\modules\v1\models\User;

/**
 * Class UsersController
 *
 * @package api\modules\v1\controllers
 */
class UsersController extends ApiActiveController
{
    public $modelClass = User::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * ActiveDataProvider for search REST API
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        $searchModel = new UserSearch();

        return $searchModel->search(Yii::$app->request->queryParams);
    }
}
