<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\models\form\AuthForm;
use yii\filters\AccessControl;
use yii\rest\Controller;

/**
 * Class UserController
 *
 * @package api\modules\v1\controllers
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // Разрешаем доступ только авторизованным пользователям
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'except' => ['login'],
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@']
                ]
            ]
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'login' => ['post']
        ];
    }

    /**
     * @SWG\Post(path="/user/login",
     *     tags={"general"},
     *     summary="Authorization user",
     *     description="Authorization user",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *        name = "email",
     *        in = "formData",
     *        description = "user email",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Parameter(
     *        name = "password",
     *        in = "formData",
     *        description = "password",
     *        required = true,
     *        type = "string"
     *     ),
     *
     *     @SWG\Response(
     *         response = 200,
     *         description = "user token"
     *     ),
     * )
     *
     */
    public function actionLogin()
    {
        $model = new AuthForm();
        $model->load(Yii::$app->request->bodyParams, '');
        if($token = $model->login()) {
            return $token;
        }

        return $model;
    }
}
