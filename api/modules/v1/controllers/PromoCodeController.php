<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\components\ApiActiveController;
use api\modules\v1\models\PromoCode;
use api\modules\v1\models\search\PromoCodeSearch;

/**
 * Class PromoCodeController
 *
 * @package api\modules\v1\controllers
 */
class PromoCodeController extends ApiActiveController
{
    public $modelClass = PromoCode::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * ActiveDataProvider for search REST API
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        $searchModel = new PromoCodeSearch();

        return $searchModel->search(Yii::$app->request->queryParams);
    }
}
