<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\components;

use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\rest\Serializer;
use yii\web\ForbiddenHttpException;

/**
 * Class ApiActiveController
 *
 * @package api\modules\v1\components
 */
class ApiActiveController extends ActiveController
{

    /** @var array обработчик данных ответа */
    public $serializer = [
        'class' => Serializer::class,
        'collectionEnvelope' => 'items',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'realm' => 'private API',
        ];

        // Разрешаем доступ только авторизованным пользователям
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@']
                ]
            ]
        ];

        return $behaviors;
    }

    /**
     * Проверка прав доступа к действиям
     *
     * @param string $action
     * @param null   $model
     * @param array  $params
     *
     * @throws ForbiddenHttpException
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        if(in_array($action, ['create', 'update', 'delete'], true)) {
            throw new ForbiddenHttpException();
        }
    }
}
