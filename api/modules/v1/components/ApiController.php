<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\components;

use yii\rest\Controller;
use yii\rest\Serializer;

/**
 * Class ApiController
 *
 * @package api\modules\v1\components
 */
class ApiController extends Controller
{

    /** @var array обработчик данных ответа */
    public $serializer = [
        'class' => Serializer::class,
        'collectionEnvelope' => 'items',
    ];
}
