<?php
namespace api\modules\v1\swagger;
/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="test-source-api.local",
 *     basePath="/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="test-source-api.local",
 *         description="Version: __1.0.0__",
 *         @SWG\Contact(name = "infinum", email = "infinum@m2store.ru")
 *     ),
 * )
 */
/**
 * @SWG\Definition(
 *   @SWG\Xml(name="##default")
 * )
 */
class ApiResponse
{
    /**
     * @SWG\Property(format="int32", description = "code of result")
     * @var int
     */
    public $code;
    /**
     * @SWG\Property
     * @var string
     */
    public $type;
    /**
     * @SWG\Property
     * @var string
     */
    public $message;
    /**
     * @SWG\Property(format = "int64", enum = {1, 2})
     * @var integer
     */
    public $status;
}