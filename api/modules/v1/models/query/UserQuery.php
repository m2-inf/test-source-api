<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\models\query;

use api\modules\v1\models\User;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\api\modules\v1\models\User]].
 *
 * @see \api\modules\v1\models\User
 */
class UserQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['active' => User::ACTIVE_YES]);
    }
}
