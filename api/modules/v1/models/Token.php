<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\models;

use api\common\models\UserToken;

class Token extends UserToken
{
    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'token' => 'token'
        ];
    }

    public function generateToken($userID)
    {
        return md5(time() . $userID . uniqid('', true) . mt_rand(0, 1000));
    }
}
