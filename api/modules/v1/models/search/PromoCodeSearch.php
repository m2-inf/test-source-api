<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\models\search;

use api\modules\v1\models\PromoCode;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class PromoCodeSearch
 *
 * @package api\modules\v1\models\search
 */
class PromoCodeSearch extends PromoCode
{

    public function formName()
    {
        return 'search';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['code'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PromoCode::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }
}
