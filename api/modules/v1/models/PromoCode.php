<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\models;

use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * Class PromoCode
 *
 * @package api\modules\v1\models
 */
class PromoCode extends \api\common\models\PromoCode implements Linkable
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id' => 'id',
            'code' => 'code',
        ];
    }

    /**
     * @inheritdoc
     */
    public function extraFields()
    {
        return [
            'user' => 'user'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['promo-code/view', 'id' => $this->id], true)
        ];
    }
}
