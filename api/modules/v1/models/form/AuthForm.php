<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\modules\v1\models\form;

use api\modules\v1\models\Token;
use api\modules\v1\models\User;
use \yii\base\Model;

/**
 * Class AuthForm
 *
 * @package api\modules\v1\models\form
 */
class AuthForm extends Model
{
    public $email;
    public $password;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['email'], 'email'],
            [['password'], 'validatePassword'],
        ];
    }

    /**
     * Валидация пароля
     *
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
           $user = $this->getUser();
           if (!$user || !$user->validatePassword($this->password)) {
               $this->addError($attribute, 'Incorrect email or password');
           }
        }
    }

    /**
     * Авторизация пользователя
     *
     * @return Token|null
     */
    public function login()
    {
        if($this->validate()) {
            $token = new Token();
            $user = $this->getUser();
            $token->user_id = $user->id;
            $token->token = $token->generateToken($user->id);
            if($token->save()) {
                return $token;
            }
        }

        return null;
    }

    /**
     * Возвращает пользователя по email
     *
     * @return User
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['email' => $this->email]);
        }

        return $this->_user;
    }
}
