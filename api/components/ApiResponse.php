<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\components;

use yii\web\Response;

/**
 * Class ApiResponse
 *
 * @package api\components
 */
class ApiResponse extends Response
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->headers->set('X-Powered-By', '<test-source-api.local>');
    }
}
