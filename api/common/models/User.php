<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $created
 * @property string $edited
 * @property string $last_login
 * @property string $email
 * @property string $name
 * @property string $pass
 * @property integer $active
 * @property integer $locked
 * @property integer $facebook_uid
 * @property integer $google_uid
 * @property string $facebook_avatar
 * @property string $google_avatar
 * @property string $facebook_name
 * @property string $google_name
 * @property string $avatar
 */
class User extends ActiveRecord
{
    const ACTIVE_YES = 1;
    const ACTIVE_NO = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(UserToken::className(), ['user_id' => 'id']);
    }
}
