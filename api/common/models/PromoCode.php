<?php
/**
 * @link http://test-source-api.local/
 * @copyright Copyright (c) 9.2017
 * @author infinum <infinum@m2store.ru>
 */

namespace api\common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%promo_code}}".
 *
 * @property integer $id
 * @property integer $promo_condition_id
 * @property integer $user_id
 * @property string $code
 * @property string $create_time
 * @property string $use_time
 * @property integer $status
 *
 * @property User $user
 */
class PromoCode extends ActiveRecord
{
    const STATUS_NOT_USED = 0;
    const STATUS_RESERVED = 1;
    const STATUS_PAID = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_code}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
