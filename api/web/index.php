<?php
$servers = require __DIR__ . '/../config/servers.php';
if (isset($servers[$_SERVER['SERVER_NAME']])) {
    define('APP_ENV', $servers[$_SERVER['SERVER_NAME']]);
} else {
    define('APP_ENV', 'live');
}

if (APP_ENV !== 'live') {
    error_reporting(E_ALL);
    ini_set('display_errors', 'on');
    define('YII_DEBUG', true);
    define('YII_TRACE_LEVEL', 3);
} else {
    define('YII_DEBUG', false);
    define('YII_TRACE_LEVEL', 0);
}

require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/../config/api.php',
    require __DIR__ . '/../config/api.local.php',
    require __DIR__ . '/../config/servers/' . APP_ENV . '/api.php'
);

(new yii\web\Application($config))->run();
