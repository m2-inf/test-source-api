<?php

$params = require __DIR__ . '/params.php';

return [
    'id' => 'app-api',
    'vendorPath' => Yii::getAlias('@vendor'),
    'basePath' => dirname(__DIR__),
    'name' => 'test-source-api',
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => \yii\web\JsonParser::class,
            ]
        ],
        'response' => [
            'class' => \api\components\ApiResponse::class,
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => yii\rest\UrlRule::class,
                    'controller' => ['v1/users', 'v1/promo-code'],
                ],
                'v1/user/login' => 'v1/user/login',
                'v1/doc' => 'v1/default/doc',
                'v1/swagger' => 'v1/default/swagger'
            ]
        ],
        'user' => [
            'identityClass' => \api\modules\v1\models\User::class,
            'enableAutoLogin' => false,
            'enableSession' => false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'modules' => [
        'v1' => [
            'class' => api\modules\v1\Module::class,
        ]
    ],
    'params' => $params,
];
